// Requires
const {Router} = require('express');
const {check} = require('express-validator');
const {loginUsuario, googleSignIn, renovarToken} = require('../controllers');
const {validandoCampos, validarJWT} = require('../middlewares');

// Variables
const router = Router();

// MAIN
// User Log In and Authentication by JWT
router.post('/login', [
    check('password', 'err-004').trim().notEmpty(),
    check('email', 'err-006').isEmail(),
    validandoCampos
]
, loginUsuario);

// Google Log In
router.post('/google', [
    check('id_token', 'err-013').trim().notEmpty(),
    validandoCampos
]
, googleSignIn);

router.get('/', validarJWT, renovarToken)

module.exports = router;
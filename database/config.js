// Requires
require('colors');
const mongoose = require('mongoose');

// Variables
const puerto = process.env.PORT;
let opcionesDB = {}
let stringConexion = `mongodb+srv://${process.env.USER_DB}:${process.env.PASS_DB}@${process.env.SERVER_DB}/${process.env.NAME_DB}`;

// MAIN
const dbConexion = async () => {
    // Conexión Base de Datos
    try {
        await mongoose.connect(stringConexion, opcionesDB);
        console.log (`MongoDB:\\> Connected to Mongo Database : ${(process.env.NAME_DB).yellow}\n`);
    } catch (err) {
        console.log('\nERROR ===================================================================================================================================================='.red);
        throw err;
    }
}

module.exports = {
    dbConexion
}
// Variables
let usuario = null;
let socketClient = null;
const url = (window.location.hostname.includes('localhost'))
            ? 'http://localhost:8080/auth/'
            : 'https://server-basedatos.herokuapp.com/auth/';
const txtUid = document.querySelector('#txtUid');
const txtMensaje = document.querySelector('#txtMensaje');
const ulUsuarios = document.querySelector('#ulUsuarios');
const ulMensajes = document.querySelector('#ulMensajes');
const btnSalir = document.querySelector('#btnSalir');

// MAIN
// Doing a **very simple** validation of token structure
// Just for fun
const validarTokenLocalStorage = async () => {
    const tokenFromLocalStorage = localStorage.getItem('chat_token') || '';
    const tokenTresEstructuras = tokenFromLocalStorage.split('.');

    if (tokenFromLocalStorage.length < 120 || tokenTresEstructuras.length != 3) {
        window.location = 'index.html'
        throw new Error('There is no Token configured');
    } else {
        const tokenInHeader = await fetch (url, {
            headers: {'auth': tokenFromLocalStorage}
        })
    
        const {usuario, id_token} = await tokenInHeader.json();
        localStorage.setItem('chat_token', id_token);
        document.title = usuario.nombre + ' ' + usuario.apellido;
        //console.log(id_token);

        await connecToSocket();
    }
};

// Socketing
const connecToSocket = async () => {
    // Establishing connection to Socket from Client
    socketClient = io({
        'extraHeaders': {
            'auth': localStorage.getItem('chat_token')
        }
    });

    // On Connection ...
    socketClient.on('connect', () => {
        console.log("Socket Online");
    });

    // On Disconnection ...
    socketClient.on('disconnect', () => {
        console.log("Socket Offline");
    });

    // Server is emiting the last X messages sent
    // Now in client ...
    socketClient.on('recibir-mensaje', listarMensajesFront);

    // Server is emiting all active chat users
    // Now in client ...
    socketClient.on('usuarios-activos', listarUsuariosFront);

    // Server is emiting a private message
    // Now in client of receiver ...
    socketClient.on('mensaje-privado', (payload) => {
        console.log(payload);
    });
}

// Listing connected users in Front
const listarUsuariosFront = (usuarios = []) => {
    let usuariosHtml = '';

    usuarios.forEach(usuario => {
        usuariosHtml += `
            <li>
                <p>
                    <h5 class="text-success">${usuario.nombre}</h5>
                    <span class="fs-6 text-muted">${usuario.uid}</span>
                </p>
            </li>
        `;
    });

    ulUsuarios.innerHTML = usuariosHtml;
};

// Listing last X messages in Front
const listarMensajesFront = (mensajes = []) => {
    let mensajesHtml = '';

    mensajes.forEach(mensaje => {
        mensajesHtml += `
            <li>
                <p>
                    <span class="text-primary">[${mensaje.nombreUsuario}] </span>
                    <span class="fs-6 text-muted">${mensaje.mensaje}</span>
                </p>
            </li>
        `;
    });

    ulMensajes.innerHTML = mensajesHtml;
};

// Catching key input on message text form
txtMensaje.addEventListener('keyup', (ev) => {
    // Give me the message and the id of user will receive it
    const mensajeAEnviar = txtMensaje.value.trim();
    const uidUsuarioQueRecibe = txtUid.value.trim();

    // If message not empty and enter was pressed - sent the message!
    if (mensajeAEnviar.length > 0 && ev.keyCode === 13) {
        socketClient.emit('enviar-mensaje', {mensajeAEnviar, uidUsuarioQueRecibe});
        txtMensaje.value = '';
    } else {
        return;
    }
})

const main = async () => {
    await validarTokenLocalStorage();
};

main();
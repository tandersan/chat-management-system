// Requires
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);

// MAIN
async function googleVerify(id_token = '') {
  //console.log(id_token);
  const ticket = await client.verifyIdToken({
    idToken: id_token,
    audience: process.env.GOOGLE_CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const {aud, email, name, picture, given_name, family_name} = ticket.getPayload();

  return {
    aud,
    email,
    name,
    picture,
    given_name,
    family_name
  }
}

module.exports = {
    googleVerify
}
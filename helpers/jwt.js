// Requires
const jwt = require('jsonwebtoken');
const User = require('../models/user');

// MAIN
const generarJWT = (uid = '') => {
    return new Promise((resolve, reject) => {
        const payload = {uid};

        jwt.sign(payload, process.env.SEMILLA_TOKEN, {
            expiresIn: process.env.EXPIRACION_TOKEN
        }, (err, token) => {
            if (err) {
                console.log(err);
                reject('No se pudo generar el token');
            } else {
                resolve(token);
            }
        })
    })
};

const comprobarJWT = async (tokenFromLocalStorage = '') => {
    const tokenTresEstructuras = tokenFromLocalStorage.split('.');
    let errors = [];

    try {
        if (tokenFromLocalStorage.length < 120 || tokenTresEstructuras.length != 3) {
            return null;
        } else {
            // Getting User ID from JWT
            const {uid} = jwt.verify(tokenFromLocalStorage, process.env.SEMILLA_TOKEN);

            // Finding User Data on DDBB with User ID
            usuario = await User.find({_id: uid, estado: true});
            if (usuario.length == 0) {
                errors.push({
                    ok: false,
                    err_code: 'err-996',
                    msg: 'User Disabled or do not exists in database'
                });
        
                return res.status(401).json({errors})
            } else {
                return usuario[0];
            }
        }
    } catch (err) {
        return null;
    }
}

module.exports = {
    generarJWT,
    comprobarJWT
}
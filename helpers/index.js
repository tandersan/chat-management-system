// Requires
const googleVerifyModules = require('./google-verify');
const jwtModules = require('./jwt');

//MAIN
module.exports = {
    ...googleVerifyModules,
    ...jwtModules
}
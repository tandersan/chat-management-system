// Requires
require('colors');
const express = require('express');
const {socketController} = require('../sockets/controller'); // Main Controller will be
const {dbConexion} = require('../database/config');

//MAIN
class Server {
    constructor() {
        this.app = express();
        this.puerto = process.env.PORT;
        this.server = require('http').createServer(this.app);
        this.io = require('socket.io')(this.server); // Lets use some sockets
        this.publicPath = __dirname + '/../' + process.env.PUBLICPATH;
        this.routePaths = {
            auth: '/auth'
        }

        //Database conexion
        this.conectarBBDD();

        //Middlewares
        this.middlewares();

        // Paths
        this.rutas();

        // Sockets
        this.sockets();
    }

    middlewares() {
        // Reading and Parsing "body" data
        this.app.use(express.json());

        // Public Directory
        this.app.use(express.static(this.publicPath));
    }

    rutas() {
        this.app.use(this.routePaths.auth, require('../routes/auth.routes'));
    }

    sockets() {
        this.io.on('connection', (socketServer) => socketController(socketServer, this.io));
    }

    subirServidor() {
        this.server.listen(this.puerto, () => {
            console.log(`\nServer On Line\nListening port : ${(this.puerto + '').yellow}`);
        });
    }

    async conectarBBDD() {
        await dbConexion();
    }
}

module.exports = Server;
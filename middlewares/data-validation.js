// Requires
const fs = require('fs');
const {validationResult} = require('express-validator');

// Variables
let listaErr = './errList.json';

// MAIN
const validandoCampos = async (req, res, next) => {
    let errors = [];
    let arrErrores = [];

    // Obtenemos la lista de errores definidos para la aplicación
    try {
        jsonErrores = JSON.parse(fs.readFileSync(listaErr, {encoding: 'utf-8'}));
    } catch (err) {
        console.log(err);
    }

    // Cuando el correo tiene un formato inválido creamos el codigo de error y el mensaje correpsondiente
    if (!validationResult(req).isEmpty()) {
        arrErrores = validationResult(req).array();
        arrErrores.forEach( objeto => {
            cod = objeto.msg;
            let errFound = jsonErrores.listErrors.find(codLista => codLista.cod === cod)
            if (typeof errFound != 'undefined') {
                errors.push({
                    ok: false,
                    err_code: errFound.cod,
                    msg: errFound.msg
                });
            }
        })
    }

    // Si Correo tiene problemas arrojamos el mensaje
    if (errors.length > 0) {
        return res.status(400).json({errors})
    }

    next();
}

module.exports = {
    validandoCampos
}
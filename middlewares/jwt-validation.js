// Requires
const jwt = require('jsonwebtoken');
const User = require('../models/user');

// MAIN
const validarJWT = async (req, res, next) => {
    const tokenEnviado = req.header('auth');
    let errors = [];

    if (!tokenEnviado) {
        errors.push({
            ok: false,
            err_code: 'err-997',
            msg: 'There is no token in the request'
        });

        return res.status(401).json({errors})
    }

    try {
        // Getting User ID from JWT
        const {uid} = jwt.verify(tokenEnviado, process.env.SEMILLA_TOKEN);

        // Finding User Data on DDBB with User ID
        usuario = await User.find({_id: uid, estado: true});
        if (usuario.length == 0) {
            errors.push({
                ok: false,
                err_code: 'err-996',
                msg: 'User Disabled or do not exists in database'
            });
    
            return res.status(401).json({errors})
        }
        req.usuario = usuario[0];

        next();
    } catch (err) {
        errors.push({
            ok: false,
            msg: err
        });

        return res.status(401).json({errors})
    }
}

module.exports = {
    validarJWT
}
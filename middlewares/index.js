// Requires
const dataValidationModules = require('./data-validation');
const jwtValidationModules = require('./jwt-validation');

//MAIN
module.exports = {
    ...dataValidationModules,
    ...jwtValidationModules
}
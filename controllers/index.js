// Requires
const authControllerModules = require('./auth.controller');

//MAIN
module.exports = {
    ...authControllerModules
}
// Requires
const {request, response} = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const {googleVerify, generarJWT} = require('../helpers');

//MAIN
// Google Sing In
const googleSignIn = async (req = request, res = response) => {
    const {id_token} = req.body;
    let errors = [];

    try {
        const {aud, given_name, family_name, email, picture} = await googleVerify(id_token);
        const existeUsuario = await User.findOne({email});

        if (!existeUsuario) {
            let user = new User({
                google: true,
                nombre: given_name,
                apellido: family_name,
                edad: 1,
                email: email,
                role: 'USER_ROLE',
                password: 'no-pass',
                img: picture
            });            

            // We save New User in our DDBB
            await user.save();
        // If User Exists but Estado = False ...
        } else if (!existeUsuario.estado) {
            errors.push({
                ok: false,
                err_code: 'err-998',
                msg: 'User Disabled'
            });

            return res.status(400).json({errors});
        }

        // Generating JWT
        // Saving JWT instead Google One due (1) we already verified Google Account
        // And (2) managing one type of token is simpler
        const token = await generarJWT(existeUsuario.id);

        res.json({
            msg:'Google Id Log In OK',
            ok: true,
            existeUsuario,
            token
        });
    } catch (err) {
        errors.push({
            ok: false,
            err_code: 'err-997',
            msg: 'Google User couldn\'t be verified'
        });

        return res.status(400).json({errors});
    }
};

// Validating that our user exist
// If Yes -> Log In
const loginUsuario = async (req = request, res = response) => {
    const {email, password} = req.body;
    let errors = [];

    try {
        const existeUsuario = await User.findOne({email});

        // Verifing that user exists
        if (!existeUsuario) {
            errors.push({
                ok: false,
                err_code: 'err-999',
                msg: '(User) and / or Password mistmatch'
            });

            return res.status(400).json({errors});
        // Verifing that the existing user still Enabled
        } else if (!existeUsuario.estado) {
            errors.push({
                ok: false,
                err_code: 'err-998',
                msg: 'User Disabled'
            });

            return res.status(400).json({errors});
        // Verifing that the password is correct
        } else if (!bcrypt.compareSync(password, existeUsuario.password)) {
            errors.push({
                ok: false,
                err_code: 'err-999',
                msg: '(User) and / or Password mistmatch'
            });

            return res.status(400).json({errors});
        };

        // Generating JWT
        const id_token = await generarJWT(existeUsuario.id);

        res.json({
            msg:'Log In OK',
            ok: true,
            usuario: existeUsuario,
            id_token
        });
    } catch (err) {
        console.log(err);
        return res.status(500);
    }
};

const renovarToken = async (req = request, res = response) => {
    const {usuario} = req;

    // Generating JWT
    const id_token = await generarJWT(usuario.id);

    res.json({
        msg:'Log In OK',
        ok: true,
        usuario,
        id_token    
    });
};

module.exports = {
    googleSignIn,
    loginUsuario,
    renovarToken
};
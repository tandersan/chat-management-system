//MAIN
class Mensajes {
    constructor(uid, nombreUsuario, mensaje) {
        this.uid = uid;
        this.nombreUsuario = nombreUsuario;
        this.mensaje = mensaje;
    }
}

class ChatMensajes {
    constructor() {
        this.mensajes = [];
        this.usuarios = {};
    }

    // Will maintain the last X messages on screen
    get ultimosMensajes() {
        this.mensajes = this.mensajes.splice(0, process.env.NUMBERMSGSCREEN);

        return this.mensajes;
    }

    // Returning Usarios Object as an Array of values
    // Example: [{User1}, {User2}, {User3}]
    get arrUsuarios() {
        return Object.values(this.usuarios);
    }

    // Sending messages ....
    enviarMensaje(uid, nombreUsuario, mensaje) {
        this.mensajes.unshift(new Mensajes(uid, nombreUsuario, mensaje));
    }

    // Usuario joins chat
    conectarUsuario(usuario) {
        this.usuarios[usuario.id] = usuario;
    }

    // Usuario leaves chat
    desconectarUsuario(id) {
        delete this.usuarios[id];
    }
}

module.exports = ChatMensajes;
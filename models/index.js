// Variables
const modeloChat = require('./chat');
const modeloUser = require('./user');

// MAIN
module.exports = {
    modeloChat,
    modeloUser
}
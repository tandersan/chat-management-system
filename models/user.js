// Requires
const mongoose = require('mongoose');

// Variables
let rolesValidos = {
    values: ['USER_ROLE', 'ADMIN_ROLE', 'SALES_ROLE'],
    message: '{VALUE} no es un ROL válido.'
}

// SCHEMA
const Schema = mongoose.Schema;

const usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'User First Name must be filled']
    },
    apellido: {
        type: String,
        trim: true,
        required: [true, 'User Last Name must be filled']
    },
    edad: {
        type: Number,
        trim: true,
        required: [true, 'User Age must be filled']
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        required: [true, 'User Email Addreess must be filled']
    },
    password: {
        type: String,
        required: [true, 'Please enter a valid password for User']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        trim: true,
        enum: rolesValidos
    },
    estado: {
        type: Boolean,
        trim: true,
        default: true
    },
    google: {
        type: Boolean,
        trim: true,
        default: false
    }
});

// Exclusión de la contraseña y que no se retorne en el JSON
usuarioSchema.methods.toJSON = function() {
    const { __v, password, _id, ...resto } = this.toObject();
    resto.uid = _id;
    return resto;
}

module.exports = mongoose.model('User', usuarioSchema);
// Requires
const {Socket} = require('socket.io');
const {comprobarJWT} = require('../helpers');
const {modeloChat} = require('../models/index');

// Variables
const chatMensajes = new modeloChat();

// MAIN
// Reducing server.js complexity by managing our Socket here
const socketController = async (socketServer = new Socket(), io) => {
    const usuario = await comprobarJWT(socketServer.handshake.headers['auth']);

    if(!usuario) {
        return socketServer.disconnect();
    } else {
        // ****************************************************************************
        // WHEN USERS CONNECT ...
        // ****************************************************************************
        // User name is added to "usuarios" class object
        chatMensajes.conectarUsuario(usuario);
        // Post addiont emiting all connected users to ... all conected users
        io.emit('usuarios-activos', chatMensajes.arrUsuarios);
        // Post addition emiting the last X messages from other users
        socketServer.emit('recibir-mensaje', chatMensajes.ultimosMensajes);
        // Connecting a private room which identifier is User Id from MongoDB
        // Why? For private messages
        socketServer.join(usuario.id);
        // ****************************************************************************

        // When the user leaves the chat ...
        socketServer.on('disconnect', () => {
            // Deleted from "usuarios" class object
            chatMensajes.desconectarUsuario(usuario.id);

            // Post deletion emiting all connected users to ... all conected users
            io.emit('usuarios-activos', chatMensajes.arrUsuarios);
        });

        socketServer.on('enviar-mensaje', (payload) => {
            // Give me the message and the id of user will receive it
            const mensajeAEnviar = payload.mensajeAEnviar;
            const uidUsuarioQueRecibe = payload.uidUsuarioQueRecibe;

            // If I added the User id for a private message - sent message to user
            if (uidUsuarioQueRecibe) {
                // Send but don't add to public list
                socketServer.to(uidUsuarioQueRecibe).emit('mensaje-privado', mensajeAEnviar);
            // If not - Sent to all
            } else {
                // Add message to the message array
                chatMensajes.enviarMensaje(usuario.id, usuario.nombre, mensajeAEnviar);
                // Send
                io.emit('recibir-mensaje', chatMensajes.ultimosMensajes);
            }

        });
    }
}

module.exports = {
    socketController
}